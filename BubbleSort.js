function bubbleSort(array)
{
 // Make a flag to check whether any elements have been swapped.
 var swapped = false;

 // Repeat everything in this do-while loop until there is an iteration
 // where no elements have been swapped, i.e., everything is in order.
 do 
 {
 // Initialise the flag to false - nothing swapped so far for this
 // iteration...
 swapped = false;
 
 // Run through the whole array 
// Run through the whole array swapping any numbers which are not
 // in order.
 for (var i = 1; i < array.length; i++)
 {    if(array[i-1] > array[i])
        {     var temp = array[i];
             array[i] = array[i-1];
            array[i-1] = temp;
               swapped = true
        }
 // This loops for every pair of consecutive values in the array.
 // Values are array[i-1] and array[i].
// If so, swap the two array elements and record the swap.
 }
 }
 while (swapped)
}
